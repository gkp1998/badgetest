# import re
# import os
#
# with open('README.md', 'r') as file:
#     data = file.readlines()
#
# # Replace with the URL here
# url = os.environ['CI_JOB_URL']
#
# # Replace with the job name here
# build_os = os.environ['CI_JOB_NAME']
# for index in range (0, len(data)):
#     line = data[index]
#     if build_os in line:
#         old_token = re.findall(r'\([^()]*\)',line )
#         print old_token
#         line = line.replace (old_token[1], "(" + url + ")")
#         data[index] = line
#
# with open('README.md', 'w') as file:
#     file.writelines( data )

import os
import re

os.system ("git clone https://gitlab.com/gkp1998/ci_readme_repo.git")
os.chdir ("ci_readme_repo")

with open('url_update.txt', 'r') as file:
    url_data = file.readlines()

build_os = []
os_url_map = {}


for line in url_data:
    st = line.split()
    os_url_map[st[0]] = st[1]
    build_os.append(st[0])

print os_url_map
os.chdir ("..")


with open('README.md', 'r') as file:
    data = file.readlines()

for index in range(0, len(data)):
    line = data[index]
    token = re.findall(r'\([^()]*\)',line )
    for i in build_os:
        if i in line:
            line = line.replace (token[1], "(" + os_url_map[i] + ")")
            data[index] = line
            break

with open('README.md', 'w') as file:
    file.writelines( data )

