| __OS__     | __BADGE__ |
|:-------:|:-------:|
|__WINDOWS_x86_64__|[![badge](https://www.dropbox.com/s/kaqi19ww0mxu4hd/WINDOWS_x86_64.svg?raw=1)](https://gitlab.com/gkp1998/cicdtesting/-/jobs/259915464)|
|__LINUX_x86_64__|[![badge](https://www.dropbox.com/s/917e7j5xxq2zuzv/LINUX_x86_64.svg?raw=1)](https://gitlab.com/gkp1998/badgetest/-/jobs/259837142)|
|__macOS_x86_64__|[![badge](https://www.dropbox.com/s/ooqxswqivs0j8qm/macOS_x86_64.svg?raw=1)](https://gitlab.com/gkp1998/cicdtesting/-/jobs/259915461)|
![b](https://gitlab.com/kptestfile/badgefolder/raw/master/LINUX.svg)